# Stage 1: 
FROM node:alpine

RUN apk --no-cache add yarn git
  
WORKDIR /var/www/app

ADD . ./

RUN yarn install --production

EXPOSE 9200

CMD ["node", "index.js"]
