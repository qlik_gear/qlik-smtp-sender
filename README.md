Qlik (internal) SMTP Sender
================

This project uses Nodemailer module (https://nodemailer.com/about/)

## Pre-reqs
- This module needs to run from a machine in the Qlik Network ( or VPN ) 
- Qlik IT needs to whitelist the IP (Qlik internal IP) of the machine 
- Node and NPM installed
- Clone project
```shell
git clone https://gitlab.com/qlik_gear/qlik-smtp-sender.git
```


## Resolve project dependencies
```shell
cd qlik-smtp-sender
npm install
```

## Run it
```shell
npm start
```

## Test it

POST: https://localhost:9200/sendemail

BODY

```json
{
    "to": "aor@qlik.com",
    "subject" :"Test from Qlik SMTP sender",
    "html": "<p>HI!</p>"
}
```

