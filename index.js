const express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    port = '9200',
    path = require('path'),
    http = require('http'),
    email = require('./email');


//const config = require('./config.json');
//const basicauth = require('basicauth-middleware');

// Config application
app.use(bodyParser.json());


// Set CORS headers: allow all origins, methods,
app.use(function(req, res, next){
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.header("Access-Control-Allow-Methods", "GET, PUT, PATCH, POST, DELETE");
    res.header("Access-Control-Allow-Headers", req.header('access-control-request-headers'));
    next();
});

app.post("/sendemail", async function(req, res){
    var body = req.body;
    var result = await email.send(body);
    if (result.success){
        res.json(result.message);
    } else {
        res.status(400).json(result.message);
    }
})


// --------- End Config application

//Server application
var server = http.createServer( app );
server.listen( port, function() {
    console.log('HTTP Server running on port ' + port );
} );