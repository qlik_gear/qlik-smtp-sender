'use strict';
const nodemailer = require('nodemailer');
const config = require('./config.json');

const FROM = '"Qlik" <no-reply@qlik.com>';

const transporter = nodemailer.createTransport(config);

async function send( data ) {

    try {
        console.log(`Qlik SMTP# Sending email with subject (${data.subject}) to (${data.to})`);
        // send mail with defined transport object
        let info = await transporter.sendMail({
            from: FROM, // sender address
            to: data.to, // list of receivers
            subject: data.subject, // Subject line
            text: data.subject, // plain text body
            html:  data.html
        });

        console.log('Qlik SMTP# ('+info.messageId+') sent to customer: ' + data.to);

        return {"success": true, "message": info};
    
    } catch (err) {
        console.log("err", err);
        return {"success": false, "message": err};
    }

}

module.exports.send = send;

